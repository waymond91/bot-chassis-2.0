# FPV Bot 2.0 - Chassis Design

This is the 2nd iteration of the robot described here: https://www.embeddedsolutions.design/project-2

This chassis was a very minimal design as more funds were allocated to the enclosure design and fabrication in the second phase of the contract. This design is simply aimed at mounting the new Jetson Nano board and custom servo PCB so that testing with the improved video stream could begin right away.

### Rendering

![Rendering](rendering.png)

### Robot Undergoing Camera Calibration

![Camera Calibration](correction-setup.png)



## Motor Sizing

A simple motor size estimation was performed given the parameters laid out in this quick sketch:

![Hand Sketch 1](MotorSizing/sizing-1.png)

![Hand Sketch 2](MotorSizing/sizing-2.png)

These equations were entered into matlab, where the the specifications of individual parts and motors could be entered in and then analyzed automatically

### Motor Requirements vs Wheel Size

![Motor Requirements](MotorSizing/MotorRequirements.jpg)

### Example Speed vs Torque Curve

![Speed Torque Curve](MotorSizing/MotorPerformace.jpg)

### Robot Mass vs Required Motor Torque

![Speed Torque Curve](MotorSizing/TorqueMass.jpg)